package com.haastattelu.data.dal.network;

import com.haastattelu.data.dal.network.model.UserResponse;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

public interface ServerApi {
    @GET("users")
    Observable<List<UserResponse>> getUsers();

    @GET("users/{id}")
    Observable<UserResponse> getUser(@Path("id") String id);
}

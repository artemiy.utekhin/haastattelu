package com.haastattelu.data.dal.network.model;

public class AddressResponse {
    public String street;
    public String suite;
    public String city;
    public String zipcode;
    public GeoResponse geo;
}

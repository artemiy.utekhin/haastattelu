package com.haastattelu.data.dal.cache;

import com.haastattelu.data.dal.network.model.UserResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple runtime cache for users, no serialization.
 * In a production app a lot of stuff should be cached into a DB
 * or at least SharedPreferences (there's a class here for that btw),
 * but this is a task for the interview :)
 *
 * By the way, I prefer Realm over SQLite, but I can live with the latter as long
 * as it's hidden behind sqlbrite or something
 */
public class UserCache {
    private List<UserResponse> users;
    private Map<Integer, UserResponse> userMap;

    public void setUsers(List<UserResponse> userResponses) {
        users = new ArrayList<>(userResponses);
        userMap = new HashMap<>();
        for (UserResponse response : userResponses)
            userMap.put(response.id, response);
    }

    public List<UserResponse> getUsers() {
        return users;
    }

    public UserResponse getUser(int id) {
        return userMap.get(id);
    }

    public boolean isCacheAvailable() {
        return users != null;
    }

    public void setUser(UserResponse response) {
        if (userMap == null)
            userMap = new HashMap<>();
        userMap.put(response.id, response);
    }
}

package com.haastattelu.data.dal.network.model;

public class UserResponse {
    public int id;
    public String username;
    public String name;
    public String email;
    public String phone;
    public String website;
    public CompanyResponse company;
    public AddressResponse address;
}

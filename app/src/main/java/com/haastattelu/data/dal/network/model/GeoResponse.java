package com.haastattelu.data.dal.network.model;

public class GeoResponse {
    // By all rights these are supposed to be doubles, but they're Strings in the response format
    // and we'll request map snapshots using string values anyway

    public String lat;
    public String lng;
}

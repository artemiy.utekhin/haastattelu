package com.haastattelu.data.dal.sharedprefs;

public interface SettingsProvider<T> {
    void commit(T model);
    T retrieve();
    T retrieveOrNew();
}

package com.haastattelu.application;

import android.content.Context;

import com.haastattelu.data.dal.cache.UserCache;
import com.haastattelu.data.dal.network.ServerApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

// TODO: clean out the glue or move it to modules and add as dependencies
@Module
public class ApplicationModule {
    @Provides
    public Context provideApplicationContext() {
        return Application.getContextOfApplication();
    }

    @Provides
    public ServerApi provideApi() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.apiEndpoint)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        return retrofit.create(ServerApi.class);
    }

    @Provides
    @Singleton
    public UserCache provideCache() {
        return new UserCache();
    }
}

package com.haastattelu.application;

import com.haastattelu.activity.detail.DetailActivityComponent;
import com.haastattelu.activity.detail.DetailActivityModule;
import com.haastattelu.activity.main.MainActivityComponent;
import com.haastattelu.activity.main.MainActivityModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        ApplicationModule.class,
})
public interface ApplicationComponent {

    // Activity scoped subcomponents
    MainActivityComponent plus(MainActivityModule mainActivityModule);
    DetailActivityComponent plus(DetailActivityModule detailActivityModule);
}

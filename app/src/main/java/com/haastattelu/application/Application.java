package com.haastattelu.application;

import android.content.Context;
import android.util.Log;

import com.haastattelu.sierokarte.BuildConfig;
import com.haastattelu.sierokarte.R;
import com.yandex.metrica.YandexMetrica;

public class Application extends android.app.Application {

    private static Application appContext;

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appContext = this;

        // Dagger component initialization
        applicationComponent = DaggerApplicationComponent.builder().build();

        // Analytics
        YandexMetrica.activate(getApplicationContext(), getString(R.string.metrics_token));
        YandexMetrica.setSessionTimeout(600);
        YandexMetrica.setCollectInstalledApps(false);
        YandexMetrica.setTrackLocationEnabled(false);
    }

    public static Context getContextOfApplication() {
        return appContext.getApplicationContext();
    }

    public static ApplicationComponent getApplicationComponent() {
        return appContext.applicationComponent;
    }
}

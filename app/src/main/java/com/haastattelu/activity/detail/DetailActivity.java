package com.haastattelu.activity.detail;

import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;

import com.haastattelu.activity.detail.fragment.DetailFragment;
import com.haastattelu.application.Application;
import com.haastattelu.sierokarte.R;

import catgirl.mvp.implementations.BaseComponentActivity;

public class DetailActivity extends BaseComponentActivity<DetailActivityComponent> {
    @Override
    public DetailActivityComponent createComponent() {
        return Application.getApplicationComponent().plus(new DetailActivityModule());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.common_activity_with_fragment);

        getComponent().inject(this);

        if(getIntent().getExtras() == null) {
            finish();
            return;
        }

        if (savedInstanceState == null) {
            ActivityCompat.postponeEnterTransition(this);
            DetailFragment fragment = new DetailFragment();
            fragment.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
        }
    }

    public void setToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
    }
}

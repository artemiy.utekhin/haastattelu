package com.haastattelu.activity.detail;

import com.haastattelu.activity.detail.fragment.DetailFragmentComponent;
import com.haastattelu.activity.detail.fragment.DetailFragmentModule;

import dagger.Subcomponent;

@Subcomponent(modules = {
        DetailActivityModule.class,
})
public interface DetailActivityComponent {
    void inject(DetailActivity detailActivity);

    DetailFragmentComponent plus(DetailFragmentModule detailFragmentModule);
}

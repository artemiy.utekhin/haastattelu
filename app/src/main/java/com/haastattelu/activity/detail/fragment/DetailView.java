package com.haastattelu.activity.detail.fragment;

public interface DetailView {
    void showData(UserDetail user);
    void showError();
    void showLoading();
}

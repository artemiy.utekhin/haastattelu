package com.haastattelu.activity.detail.fragment;

public class UserDetail {
    public int id;
    public String username;
    public String name;
    public String email;
    public String phone;
    public String website;

    public String companyName;
    public String catchPhrase;
    public String bs;

    public String lat;
    public String lng;

    public String street;
    public String suite;
    public String city;
    public String zipcode;
}

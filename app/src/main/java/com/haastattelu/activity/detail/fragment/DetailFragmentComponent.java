package com.haastattelu.activity.detail.fragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        DetailFragmentModule.class,
})
public interface DetailFragmentComponent {
    void inject(DetailFragment fragment);
}

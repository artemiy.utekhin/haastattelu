package com.haastattelu.activity.detail.fragment;

import com.haastattelu.data.dal.cache.UserCache;
import com.haastattelu.data.dal.network.ServerApi;
import com.haastattelu.data.dal.network.model.UserResponse;

import rx.Observable;

public class DetailProvider {
    private final ServerApi api;
    private final UserCache cache;

    // In a normal app I'd rather wrap it in a repository pattern
    DetailProvider(ServerApi api, UserCache cache) {
        this.api = api;
        this.cache = cache;
    }

    Observable<UserDetail> getUser(int id) {
        UserResponse response = cache.getUser(id);
        if (response != null)
            return Observable.just(convert(response));

        return api.getUser(String.valueOf(id)).doOnNext(cache::setUser).map(this::convert);
    }

    private UserDetail convert(UserResponse response) {
        UserDetail user = new UserDetail();

        user.name = response.name;
        user.username = response.username;
        user.email = response.email;
        user.phone = response.phone;
        user.companyName = response.company.name;
        user.catchPhrase = response.company.catchPhrase;
        user.city = response.address.city;
        user.zipcode = response.address.zipcode;
        user.street = response.address.street;
        user.suite = response.address.suite;
        user.lat = response.address.geo.lat;
        user.lng = response.address.geo.lng;
        user.website =
                response.website.startsWith("http://") || response.website.startsWith("https://")
                ? response.website
                : "http://" + response.website;

        return user;
    }
}

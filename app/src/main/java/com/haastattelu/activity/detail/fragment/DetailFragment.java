package com.haastattelu.activity.detail.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.haastattelu.activity.detail.DetailActivity;
import com.haastattelu.activity.detail.DetailActivityModule;
import com.haastattelu.application.Application;
import com.haastattelu.sierokarte.R;
import com.squareup.picasso.Picasso;

import net.opacapp.multilinecollapsingtoolbar.CollapsingToolbarLayout;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import catgirl.mvp.implementations.BasePresenterFragment;

public class DetailFragment
        extends BasePresenterFragment<DetailPresenter, DetailFragmentComponent>
        implements DetailView
{
    public static final String USER_ID = "userId";

    // MVP

    @Inject DetailPresenter presenter;

    @Override
    protected DetailFragmentComponent createComponent() {
        return Application.getApplicationComponent().plus(new DetailActivityModule()).plus(new DetailFragmentModule());
    }

    @Override
    protected void onComponentCreated() {
        getComponent().inject(this);
    }

    @Override
    protected DetailPresenter createPresenter() {
        // Or I could inject a PresenterFactory
        presenter.setId(getArguments().getInt(USER_ID));
        return presenter;
    }

    // View

    @Bind(R.id.Toolbar) Toolbar toolbar;
    @Bind(R.id.CollapsingToolbar) CollapsingToolbarLayout collapsingToolbar;
    @Bind(R.id.Content) View content;
    @Bind(R.id.ErrorLayout) View error;
    @Bind(R.id.Loading) View loading;

    @Bind(R.id.Detail_Address1) TextView address1;
    @Bind(R.id.Detail_Address2) TextView address2;
    @Bind(R.id.Detail_Catchphrase) TextView catchphrase;
    @Bind(R.id.Detail_CompanyName) TextView companyName;
    @Bind(R.id.Detail_Email) TextView email;
    @Bind(R.id.Detail_Username) TextView username;

    @Bind(R.id.Detail_EmailLayout) View emailLayout;
    @Bind(R.id.Detail_Directions) View directionsLayout;
    @Bind(R.id.Detail_Phone) View phoneLayout;
    @Bind(R.id.Detail_Website) View websiteLayout;
    @Bind(R.id.Detail_AddressLayout) View addressLayout;

    @Bind(R.id.Detail_Map) ImageView map;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_fragment, container, false);

        ButterKnife.bind(this, view);

        ((DetailActivity) getActivity()).setToolbar(toolbar);

        return view;
    }

    @Override
    public void showData(UserDetail user) {
        content.setVisibility(View.VISIBLE);
        loading.setVisibility(View.GONE);
        error.setVisibility(View.GONE);

        collapsingToolbar.setTitle(user.name);

        address1.setText(user.street + ", " + user.suite);
        address2.setText(user.zipcode + ", " + user.city);
        companyName.setText(user.companyName);
        catchphrase.setText(user.catchPhrase);
        email.setText(user.email);
        username.setText(user.username);

        emailLayout.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:"));
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{user.email});
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(intent);
            }
        });

        websiteLayout.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(user.website));
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(intent);
            }
        });

        phoneLayout.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + user.phone));
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(intent);
            }
        });

        View.OnClickListener directionsListener = view -> {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?f=d&daddr=" + user.lat + "," + user.lng));
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(intent);
            }
        };

        directionsLayout.setOnClickListener(directionsListener);
        addressLayout.setOnClickListener(directionsListener);
        map.setOnClickListener(directionsListener);

        String derp = "http://maps.googleapis.com/maps/api/staticmap?key="
                + getActivity().getString(R.string.google_api_key)
                + "&markers=color:green%7C" + user.lat + "," + user.lng
                + "&zoom=11&size=900x600";
        Log.v("Derp", derp);

        Picasso.with(getActivity()).load(
                "http://maps.googleapis.com/maps/api/staticmap?key="
                        + getActivity().getString(R.string.google_api_key)
                        + "&markers=color:green%7C" + user.lat + "," + user.lng
                        + "&zoom=11&size=900x600").into(map);
    }

    @Override
    public void showError() {
        content.setVisibility(View.GONE);
        loading.setVisibility(View.GONE);
        error.setVisibility(View.VISIBLE);
        error.findViewById(R.id.ReloadButton).setOnClickListener(
                view -> getPresenter().loadData());
    }

    @Override
    public void showLoading() {
        content.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);
        error.setVisibility(View.GONE);
    }
}

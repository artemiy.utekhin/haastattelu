package com.haastattelu.activity.detail.fragment;

import catgirl.mvp.implementations.BasePresenter;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class DetailPresenter extends BasePresenter<DetailView> {
    private int id;
    private DetailProvider provider;

    private boolean showError;
    private UserDetail data;

    public DetailPresenter(DetailProvider provider) {
        this.provider = provider;
    }

    public void setId(int id) {
        this.id = id;
        loadData();
    }

    public void loadData() {
        provider.getUser(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        result -> {
                            data = result;
                            if (getView() != null)
                                getView().showData(data);
                        }, error -> {
                            error.printStackTrace();
                            showError = true;
                            if (getView() != null)
                                getView().showError();
                        }
                );
    }

    @Override
    public void bindView(DetailView view) {
        super.bindView(view);

        if (showError)
            view.showError();
        else if (data != null)
            view.showData(data);
    }
}

package com.haastattelu.activity.detail.fragment;

import com.haastattelu.data.dal.cache.UserCache;
import com.haastattelu.data.dal.network.ServerApi;

import dagger.Module;
import dagger.Provides;

@Module
public class DetailFragmentModule {
    @Provides
    public DetailProvider provideProvider(ServerApi api, UserCache cache) {
        return new DetailProvider(api, cache);
    }

    @Provides
    public DetailPresenter providePresenter(DetailProvider provider) {
        return new DetailPresenter(provider);
    }
}

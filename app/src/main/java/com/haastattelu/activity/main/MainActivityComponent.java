package com.haastattelu.activity.main;

import com.haastattelu.activity.main.fragment.UsersFragmentComponent;
import com.haastattelu.activity.main.fragment.UsersFragmentModule;

import dagger.Subcomponent;

@Subcomponent(modules = {
        MainActivityModule.class
})
public interface MainActivityComponent {
    void inject(MainActivity mainActivity);

    UsersFragmentComponent plus(UsersFragmentModule usersFragmentModule);
}

package com.haastattelu.activity.main.fragment;

import android.text.TextUtils;

import com.haastattelu.data.dal.cache.UserCache;
import com.haastattelu.data.dal.network.ServerApi;
import com.haastattelu.data.dal.network.model.UserResponse;

import java.util.ArrayList;
import java.util.List;

import rx.Notification;
import rx.Observable;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

public class UsersProvider {
    private ServerApi api;
    private UserCache cache;

    private String query;
    private List<User> users;
    private PublishSubject<Notification<List<User>>> subject = PublishSubject.create();

    UsersProvider(ServerApi api, UserCache cache) {
        this.api = api;
        this.cache = cache;

        // Start with cached users if available
        if (cache.isCacheAvailable())
            users = convert(cache.getUsers());
    }

    public Observable<Notification<List<User>>> subscribe() {
        return subject.doOnSubscribe(() -> {
            // Send cached users if available
            if (users != null)
                subject.onNext(Notification.createOnNext(users));
        });
    }

    void requestReload() {
        // Yet to figure out a good way to handle Schedulers here, their place is in Presenter
        api.getUsers()
                .doOnNext(cache::setUsers)
                .map(this::convert)
                .subscribeOn(Schedulers.io())
                .subscribe(
                        result -> {
                            users = result;
                            subject.onNext(Notification.createOnNext(filter(result, query)));
                        }, error -> {
                            subject.onNext(Notification.createOnError(error));
                        });
    }

    // Trivial search, nothing to do with real world search
    private List<User> filter(List<User> users, String query) {
        if (TextUtils.isEmpty(query))
            return users;

        List<User> results = new ArrayList<>();
        for (User user : users) {
            if (user.name.toLowerCase().contains(query) || user.companyName.toLowerCase().contains(query))
                results.add(user);
        }
        return results;
    }

    void setQuery(String query) {
        this.query = query.toLowerCase();
        if (users != null) {
            subject.onNext(Notification.createOnNext(filter(users, query)));
        }
    }

    List<User> convert(List<UserResponse> userResponses) {
        List<User> result = new ArrayList<>();
        for (UserResponse response : userResponses) {
            User user = new User();
            if (response.company != null) {
                // Actually the opposite will never happen with this demo API, but it's a good habit
                user.companyName = response.company.name;
            }
            user.name = response.name;
            user.id = response.id;
            result.add(user);
        }
        return result;
    }
}

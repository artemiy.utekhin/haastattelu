package com.haastattelu.activity.main;

import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;

import com.haastattelu.activity.main.fragment.UsersFragment;
import com.haastattelu.application.Application;
import com.haastattelu.sierokarte.R;

import catgirl.mvp.implementations.BaseComponentActivity;

public class MainActivity extends BaseComponentActivity<MainActivityComponent> {
    @Override
    public MainActivityComponent createComponent() {
        return Application.getApplicationComponent().plus(new MainActivityModule());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.common_activity_with_fragment);

        getComponent().inject(this);

        if (savedInstanceState == null) {
            UsersFragment fragment = new UsersFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
        }
    }

    public void setToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}

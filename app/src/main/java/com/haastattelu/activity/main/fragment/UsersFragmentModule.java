package com.haastattelu.activity.main.fragment;

import com.haastattelu.data.dal.cache.UserCache;
import com.haastattelu.data.dal.network.ServerApi;

import dagger.Module;
import dagger.Provides;

@Module
public class UsersFragmentModule {
    @Provides
    public UsersPresenter providePresenter(UsersProvider provider) {
        return new UsersPresenter(provider);
    }

    @Provides
    public UsersProvider provideProvider(ServerApi api, UserCache cache) {
        return new UsersProvider(api, cache);
    }
}

package com.haastattelu.activity.main.fragment;

import java.util.ArrayList;
import java.util.List;

import catgirl.mvp.implementations.BasePresenter;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public class UsersPresenter extends BasePresenter<UsersView> {
    private UsersProvider provider;
    private List<User> items = new ArrayList<>();

    private boolean dataLoaded = false;
    private boolean isLoading = true;
    private boolean showError = false;

    private Subscription subscription;

    UsersPresenter(UsersProvider provider) {
        this.provider = provider;
        subscribe();
        provider.requestReload();
    }

    private void subscribe() {
        subscription = provider.subscribe()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        response -> {
                            switch(response.getKind()) {
                                case OnNext:
                                    // Consider the incoming data immutable just in case
                                    items = new ArrayList<>(response.getValue());
                                    dataLoaded = true;

                                    subscription = null;
                                    isLoading = false;
                                    if (getView() != null) {
                                        if (!dataLoaded)
                                            getView().showMoreItems(items, true);
                                        else
                                            getView().showExistingItems(items, true);
                                    }
                                    break;
                                case OnError:
                                    isLoading = false;
                                    if (getView() != null) {
                                        if (!dataLoaded) {
                                            showError = true;
                                            getView().showMoreItemsError(true);
                                        }
                                        else
                                            getView().showNewItemsError();
                                    }
                                    break;
                            }
                        }
                );
    }

    @Override
    public void onDestroy() {
        if (subscription != null)
            subscription.unsubscribe();
    }

    @Override
    public void bindView(UsersView view) {
        super.bindView(view);

        if (isLoading) {
            getView().showLoadingNewItems();
        }

        if (dataLoaded) {
            getView().showExistingItems(items, true);
        } else if (showError) {
            getView().showMoreItemsError(false);
        }
    }

    User getItem(int position) {
        return items.get(position);
    }

    int getItemCount() {
        return items.size();
    }

    void loadItems() {
        if (isLoading)
            return;

        isLoading = true;
        showError = false;

        if (getView() != null)
            getView().showLoadingNewItems();

        provider.requestReload();
    }

    public void setQuery(String query) {
        provider.setQuery(query);
    }

    public void onItemClicked(int position) {
        if (getView() != null)
            getView().gotoDetailActivity(items.get(position).id);
    }
}

package com.haastattelu.activity.main.fragment;

import dagger.Subcomponent;

@Subcomponent(modules = {
        UsersFragmentModule.class,
})
public interface UsersFragmentComponent {
    void inject(UsersFragment usersFragment);
}

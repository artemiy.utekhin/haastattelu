package com.haastattelu.activity.main.fragment;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.haastattelu.sierokarte.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class UserViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.User_Name) TextView name;
    @Bind(R.id.User_CompanyName) TextView companyName;
    private View itemView;

    public UserViewHolder(View itemView) {
        super(itemView);
        this.itemView = itemView;
        ButterKnife.bind(this, itemView);
    }

    public void bind(User item, View.OnClickListener onClickListener) {
        itemView.setOnClickListener(onClickListener);
        name.setText(item.name);
        companyName.setText(item.companyName);
    }
}

package com.haastattelu.activity.main.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.haastattelu.activity.detail.DetailActivity;
import com.haastattelu.activity.detail.fragment.DetailFragment;
import com.haastattelu.activity.main.MainActivity;
import com.haastattelu.activity.main.MainActivityModule;
import com.haastattelu.application.Application;
import com.haastattelu.common.view.LazyLoadFragment;
import com.haastattelu.common.widgets.ClearableAutoCompleteTextView;
import com.haastattelu.common.widgets.SimpleTextWatcher;
import com.haastattelu.sierokarte.R;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class UsersFragment
        extends LazyLoadFragment<User, UsersPresenter, UsersFragmentComponent>
        implements UsersView
{

    // MVP boilerplate

    @Inject
    UsersPresenter presenter;

    @Override
    protected UsersFragmentComponent createComponent() {
        return Application.getApplicationComponent().plus(new MainActivityModule()).plus(new UsersFragmentModule());
    }

    @Override
    protected void onComponentCreated() {
        getComponent().inject(this);
    }

    @Override
    protected UsersPresenter createPresenter() {
        return presenter;
    }

    // View implementation

    @Bind(R.id.Toolbar) Toolbar toolbar;
    @Bind(R.id.SearchInput) ClearableAutoCompleteTextView searchInput;

    final static String SEARCH_OPEN = "searchOpen";
    boolean searchOpen = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        ButterKnife.bind(this, view);

        // Hack for proper toolbar items placement between pre- and post-Lollipop
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                ViewGroup.LayoutParams params = toolbar.getLayoutParams();
                params.height += getResources().getDimensionPixelSize(resourceId);
                toolbar.setLayoutParams(params);
                toolbar.setPadding(
                        toolbar.getPaddingLeft(),
                        toolbar.getPaddingTop()
                                + getResources().getDimensionPixelSize(resourceId),
                        toolbar.getPaddingRight(),
                        toolbar.getPaddingBottom());
            }
        }

        ((MainActivity) getActivity()).setToolbar(toolbar);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(R.string.users_toolbar_title);

        searchInput.setOnClearListener(() -> {
            searchInput.setVisibility(View.GONE);
            searchInput.setText("");
            getPresenter().setQuery("");
            searchOpen = false;
            getActivity().invalidateOptionsMenu();
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY,0);
        });

        searchInput.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (searchInput.getText() != null)
                    getPresenter().setQuery(searchInput.getText().toString());
            }
        });

        searchInput.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchInput.clearFocus();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY,0);
                return true;
            }
            return false;
        });

        return view;
    }

    @Override
    protected int getItemCount() {
        return getPresenter().getItemCount();
    }

    @Override
    protected long getItemId(int position) {
        return getPresenter().getItem(position).id;
    }

    @Override
    protected int getItemViewType(int position) {
        return 0;
    }

    @Override
    protected void loadNew() {
        getPresenter().loadItems();
    }

    @Override
    protected void loadMore() {

    }

    @Override
    protected RecyclerView.ViewHolder createViewHolder(ViewGroup parent, int viewType) {
        return new UserViewHolder(getActivity().getLayoutInflater().inflate(R.layout.users_item, parent, false));
    }

    @Override
    protected void bindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((UserViewHolder) holder).bind(getPresenter().getItem(position), view -> getPresenter().onItemClicked(position));
    }

    @Override
    protected RecyclerView.ViewHolder createErrorViewHolder(ViewGroup parent) {
        View error = getActivity().getLayoutInflater().inflate(R.layout.common_item_error_try_again, parent, false);
        error.findViewById(R.id.ReloadButton).setOnClickListener(
                view -> getPresenter().loadItems());
        return new RecyclerView.ViewHolder(error) {
        };
    }

    @Override
    protected View getEmptyMessage(ViewGroup parent) {
        return getActivity().getLayoutInflater().inflate(R.layout.common_item_empty, parent, false);
    }

    @Override
    protected void showMoreItemsErrorToast() {
        Toast.makeText(getActivity(), R.string.common_loading_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void showNewItemsErrorToast() {
        showMoreItemsErrorToast();
    }

    @Override
    protected boolean hasStableIds() {
        return true;
    }

    @Override
    protected int getSpanCount() {
        return 1;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.users_fragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(SEARCH_OPEN, searchOpen);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            searchOpen = savedInstanceState.getBoolean(SEARCH_OPEN, false);

            if (searchOpen)
                searchInput.setVisibility(View.VISIBLE);
            else
                searchInput.setVisibility(View.GONE);

            getActivity().invalidateOptionsMenu();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        menu.clear();
        if (!searchOpen)
            inflater.inflate(R.menu.users_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.MenuItem_Search:
                searchInput.setVisibility(View.VISIBLE);
                searchInput.requestFocus();
                searchOpen = true;
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);
                getActivity().invalidateOptionsMenu();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void gotoDetailActivity(int id) {
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(DetailFragment.USER_ID, id);
        startActivity(intent);
    }
}

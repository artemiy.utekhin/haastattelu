package com.haastattelu.activity.main.fragment;

import com.haastattelu.common.view.LazyLoadView;

public interface UsersView extends LazyLoadView<User> {
    void gotoDetailActivity(int id);
}
